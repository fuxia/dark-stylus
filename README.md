# Dark Stylus

A user stylesheet for the [Stylus user stylesheet extension](https://add0n.com/stylus.html)!

With this user stylesheet you can customize the Stylus editor, and even the 
little configuration popup.

Options include:

- background color
- text color
- link color
- code font size
- code line-height
	
The default color scheme is a neutral dark grey, but you can use any dark 
scheme you want. It will not work so well with a light background.

Click on the install link below, and get updates automatically whenever a 
change is needed. Report bugs, and feature suggestions in the 
[issue tracker](https://gitlab.com/fuxia/dark-stylus/-/issues).

**[Install](https://gitlab.com/fuxia/dark-stylus/-/raw/master/stylus.user.css)**

## Screenshots

Default color scheme

![default scheme](img/default.png)

Blue color scheme

![blue scheme](img/blue.png)

Red color scheme, including popup style

![red scheme](img/red.png)
![red scheme](img/config.red.png)